
document.addEventListener("deviceready", onDeviceReady, false);

var store;
var $status;
var serverURL = "http://192.168.20.106/sancharimitra/server/";
var dataURL = "data/";
var BussesDBName = "busses.json";
var LocationsDBName = "locations.json";
var locations;
var busses;
var DirectRoutes = [];
var IndirectRoutes = [];
var IntermediateLocations = [];
var DirectRouteFound_flag = true;
var newRoute;
var newRouteI;
var Route = [];

function onDeviceReady() {

  //alert('Device ready');

  store = cordova.file.dataDirectory;
  window.resolveLocalFileSystemURL(store + BussesDBName, Updater, downloadDB);

  $.get(store + LocationsDBName, DisplayLocations);
  $.get(store + BussesDBName, function(busses_json) {

    busses = JSON.parse(busses_json);

  });

  $('#searchBtn').click(function() {

    var source = $('#sourceOption').val();
    var destination = $('#destinationOption').val();

    GenerateRoute(source, destination);

  });

}

function downloadFile(Url, FileName) {

  var fileTransfer = new FileTransfer();

  fileTransfer.download(Url + FileName, store + FileName,
    function(entry) {

      alert('Database downloaded (' + FileName + ') \nInitialising..');

    },
    function(err) {

      alert('Error in downloading ' + FileName + '\n' + JSON.stringify(err));

    }
  );
}

function downloadDB() {

  var Url = serverURL + dataURL;

  downloadFile(Url, BussesDBName);
  downloadFile(Url, LocationsDBName);

  $.get(serverURL + "GetHash.php", function(hash) {

    var prefs = plugins.appPreferences;
    //alert('busses: ' + hash);
    prefs.store(HashStored, GenericErrorHandler, 'hash', hash);

  });
}

function HashStored(status) {

  alert("Database has been setup and ready to use" + JSON.stringify(status));//not necessary to show status

}

function Updater() {

  var prefs = plugins.appPreferences;
  prefs.fetch (HashHandler, GenericErrorHandler, 'hash');

}

function HashHandler(storedHash) {

  $.get(serverURL + "GetHash.php", function(newHash) {

    var storedHashString = new String(storedHash);
    var newHashString = new String(newHash);
    var diff = storedHashString.localeCompare(newHashString);

    if(diff != 0)
      if(confirm("New Update available!\nDo you want to update?"))
        downloadDB();

  });

}

function DisplayLocations(locs_json) {

  locations = JSON.parse(locs_json);
  var option = "";

  for(var i = 0; i < locations.length; i++) {
    option += '<option value="' + locations[i].name + '">' + locations[i].name + '</option>';

  }

  $('#sourceOption').append(option);
  $('#destinationOption').append(option);

}

function GenerateRoute(source, destination) {
  alert('sou: ' + source + '\ndest: ' + destination);

  if(source.localeCompare(destination) != 0) {

    GenerateDirectRoute(source, destination);

    if(!DirectRouteFound_flag)
      GenerateInDirectRoute(source, destination);

    Displayer();

  }
  else
    alert('Source and destination are same? lol!');

}

function GenerateDirectRoute(source, destination) {

  //alert('calcing direct route..');

  var sourceBusStops = [];
  var destinationBusStops = [];

  for(var i = 0; i < locations.length; i++) {

    if(source.localeCompare(locations[i].name) == 0)
      sourceBusStops = locations[i].busNumbers;
    if(destination.localeCompare(locations[i].name) == 0)
      destinationBusStops = locations[i].busNumbers;

  }

  var PossibleDirectBusses = _.intersection(sourceBusStops,destinationBusStops);

  if(PossibleDirectBusses.length == 0) {

    DirectRouteFound_flag = false;
    alert("Sorry no direct route available.\nFinding indirect route now.");
    return;

  }

  //alert(PossibleDirectBusses);

  for(var i = 0; i < PossibleDirectBusses.length; i++) {

    if(!DirectRouteFound_flag | true) { //remove the "| true" to get detailed route in case of direct route

      newRoute = new Object();

      newRoute.number = PossibleDirectBusses[i];
      newRoute.route = [];
      newRoute.route.push(source);
      newRoute.route.push(destination);

      DirectRoutes.push(newRoute);

    }
    else {

      newRoute = _.find(busses, function(bus) {

        if(PossibleDirectBusses[i].localeCompare(bus.number) == 0)
          return bus.route;

      });

      var SourceIndex = _.indexOf(newRoute.route, source);
      newRoute.route.splice(0, SourceIndex);

      var DestinationIndex = _.indexOf(newRoute.route, destination);
      newRoute.route.splice(DestinationIndex + 1, newRoute.route.length);

      DirectRoutes.push(newRoute);

    }

  }

  //alert(JSON.stringify(DirectRoutes));

}

function GenerateInDirectRoute(source, destination) {

  alert('calcing indirect routes..');

  var sourceBusStopsI = [];
  var destinationBusStopsI = [];
  var SourceBusRoutes = [];
  var DestinationBusRoutes = [];
  var PossibleIntermediateLocations = [];


  for(var i = 0; i < locations.length; i++) {

    if(source.localeCompare(locations[i].name) == 0)
      sourceBusStopsI = locations[i].busNumbers;

    if(destination.localeCompare(locations[i].name) == 0)
      destinationBusStopsI = locations[i].busNumbers;
  }

  /* (was)Commented cause better code which does the same thing follows*/
  for(var i = 0; i < sourceBusStopsI.length; i++) {

    for(var j = 0; j < busses.length; j++) {

      if(sourceBusStopsI[i].localeCompare(busses[j].number) == 0)
        SourceBusRoutes.push(busses[j]);

    }

  }
  for(var i = 0; i < destinationBusStopsI.length; i++) {

    for(var j = 0; j < busses.length; j++) {

      if(destinationBusStopsI[i].localeCompare(busses[j].number) == 0)
        DestinationBusRoutes.push(busses[j]);

    }

  }

  /* on second thought, doing this is not a good idea
  var RequiredBusses = [];
  RequiredBusses = _.union(sourceBusStops,destinationBusStops);

  for(var i = 0; i < RequiredBusses.length; i++) {

    for(var j = 0; j < busses.length; j++) {

      if(RequiredBusses[i].localeCompare(busses[j].number) == 0)
        RequiredBusRoutes.push(busses[j]);
    }
  }
  */

  for(var i = 0; i < SourceBusRoutes.length; i++) {

    for(var j = 0; j < DestinationBusRoutes.length; j++) {

      PossibleIntermediateLocations = _.intersection(SourceBusRoutes[i].route, DestinationBusRoutes[j].route);

    }

  }

  //alert('source '+JSON.stringify(SourceBusRoutes)+' \ndest '+JSON.stringify(DestinationBusRoutes));

  IntermediateLocations = _.uniq(PossibleIntermediateLocations);

  //alert(JSON.stringify(IntermediateLocations));

  for(var i = 0; i < IntermediateLocations.length; i++) {

    newRouteI = new Object();

    GenerateDirectRoute(source, IntermediateLocations[i]);
    newRouteI.first = _.clone(DirectRoutes);
    DirectRoutes.length = 0;

    GenerateDirectRoute(IntermediateLocations[i], destination);
    newRouteI.second = _.clone(DirectRoutes);
    DirectRoutes.length = 0;

    IndirectRoutes.push(newRouteI);
    //alert(JSON.stringify(newRouteI));

  }

  //alert(JSON.stringify(IndirectRoutes));

}

function Displayer() {

  //for(var i = 0; i < DirectRoutes.length; i++)
    //alert(DirectRoutes[i].number + '--' + DirectRoutes[i].route);
  //

  var output = "";

  if(DirectRouteFound_flag){

    for(var i = 0; i < DirectRoutes.length; i++) {

      output += "number: ";
      output += DirectRoutes[i].number + "\n  ";
      output += " bus route: ";

      for(var j = 0; j < DirectRoutes[i].route.length; j++) {

        output += DirectRoutes[i].route[j] + '->';
      }

      output += "\n";

    }

  }
  else{
    var busses1 = [];
    var busses2 = [];

    for(var i = 0; i < IntermediateLocations.length; i++) {

      output += "To reach intermediate location: \n";
      output += IntermediateLocations[i] + "\n";

      for(var j = 0; j < IndirectRoutes.length; j++) {

        for(var k = 0; k < IndirectRoutes[j].first.length; k++) {

          busses1.push(IndirectRoutes[j].first[k].number);

        }

      }
      output += JSON.stringify(_.uniq(busses1)) + "\n";

      output += "To reach destination: \n";

      for(var j = 0; j < IndirectRoutes.length; j++) {

        for(var k = 0; k < IndirectRoutes[j].second.length; k++) {

          busses2.push(IndirectRoutes[j].second[k].number);

        }

      }
      output += JSON.stringify(_.uniq(busses2)) + "\n";

      output += "\n";

    }

  }
  alert(output);

  Refresh();
}

function Refresh() {

  DirectRoutes.length = 0;
  Routes.length = 0;
  newRoute.length = 0;
  DirectRouteFound_flag = true;
  onDeviceReady();
}

function GenericErrorHandler(error) {

  alert("ERROR: Something went wrong\n" + JSON.stringify(error));
}
