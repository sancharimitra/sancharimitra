<?php

require("functions.php");

// Main controller for the app on the browser

// Will have to handle GET requests (AJAX calls)

if(isset($_GET['request'])) {

  $request = $_GET['request'];

  if(isset($_GET['source']))
    $source = $_GET['source'];
  else
    $source = " ";

  if(isset($_GET['destination']))
    $destination = $_GET['destination'];
  else
    $destination = " ";

  switch($request) {

    case "test" : echo HelloTest($request);
                  break;
    case "generateRoute"  : echo GenerateRoute($source, $destination);
                            break;

    default : die("Server error.. Please try again");
      break;
  }
  }
?>
