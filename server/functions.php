<?php

require("classes.php");
include_once("libs/simple_html_dom.php");
include_once("underscore/underscore.php");
function getBusIndices() {

  // Stores the mitra site url
  $url        = "http://mitra.ksrtc.in/MysoreMBus/rtemap.jsp";
  // Stores the RegEx to filter html data
  $filter_rx  = "/^[0-9]+(.)*/";
  // To store the busses info
  $BusIndices = array();

  // Get data from URL
  $html = file_get_html($url);

  foreach($html->find('option') as $busIndex) {

    if(preg_match($filter_rx, $busIndex->plaintext)) {

      //data is $busIndex->value (actual index) and $busIndex->plaintext (bus number which needs to be trimmed)
      $busNumber = explode(" ", $busIndex->plaintext);
      $bus = new Bus();
      $bus->index   = $busIndex->value;
      $bus->number  = $busNumber[0] . " " . ltrim($busNumber[1]);
      array_push($BusIndices, $bus);
    }
  }
  return $BusIndices;
}

function fetchBusRoute($BusIndex) {

  $url = "http://mitra.ksrtc.in/MysoreMBus/rtemap.jsp?count=" . $BusIndex;

  $html = file_get_html($url);

  $BusRoute = array();

  foreach($html->find('td') as $route) {
    if($route->plaintext != "") {
      array_push($BusRoute, $route->plaintext);
    }
  }
  return $BusRoute;
}

function saveToJSONFile($JSONData, $file) {

  $file_handler = fopen("data/" . $file, "w+");
  $status = fwrite($file_handler, $JSONData);

  if($status <= 0)
    return false;
  else {
    fclose($file_handler);
    return true;
  }
}

function UpdateBussesDB() {

  $errorFlag = true;
  $bussesDB = "busses.json";
  //$busIndices = getBusIndices();
  $busIndices = array();
  $b = new Bus();
  $b->index = 66;
  $b->number = 123;
  array_push($busIndices, $b);
  $b = new Bus();
  $b->index = 67;
  $b->number = 124;
  array_push($busIndices, $b);

  $busRouteInfo = array();

  foreach($busIndices as $bi) {

    $busRoute = fetchBusRoute($bi->index);

    $bri = new BusRouteInfo();
    $bri->number = "$bi->number";

    foreach($busRoute as $b) {

      $bri->addRoute($b);
    }
    array_push($busRouteInfo, $bri);
  }

  $busRouteInfo_json = json_encode($busRouteInfo);

  if(!saveToJSONFile($busRouteInfo_json, $bussesDB))
    die("ERROR: Error updating database");

  return $errorFlag; //error handling yet to be setup
}

function UpdateLocationsDB() {

  $errorFlag = true;

  $locationsDB = "locations.json";
  $bussesDB = "data/busses.json"; //remove the ".bak" in production

  $LocationPresent_flag = false;
  $busNumberPresent_flag = false;

  $busses_file_handler = fopen($bussesDB, "r");

  $busRouteInfo_json = fread($busses_file_handler, filesize($bussesDB));

  $busRouteInfo = json_decode($busRouteInfo_json);

  $locationBus = array();

  foreach($busRouteInfo as $bri) {

    foreach($bri->route as $route) {

      if($locationBus) {

        foreach($locationBus as $lb) {

          if($route == $lb->name) {
            $LocationPresent_flag = true;

            if(!in_array($bri->number, $lb->busNumbers))
              $lb->addNumber($bri->number);

          }
        }
      }

      if(!$LocationPresent_flag) {

        $newLocationBus = new LocationBus();
        $newLocationBus->name = $route;
        $newLocationBus->addNumber($bri->number);
        array_push($locationBus, $newLocationBus);
      }

      $LocationPresent_flag = false;
    }
  }

  $locationBus_json = json_encode($locationBus);

  if(!saveToJSONFile($locationBus_json, $locationsDB))
    die("ERROR: Error saving the locations.json");

  return $errorFlag; //error handling yet to be setup
}


function getHash() {

  return md5_file("data/busses.json");
  //return "lol";                           //for testing purposes!

}

function HelloTest($req) {

  return "Hello people!..it works and you requested for " . $req;
}

$Route = array();
$PossibleDirectBusses = array();
$DirectRouteFound_flag = true;

function GenerateRoute($source, $destination)
{

  global $DirectRouteFound_flag;

  if(strcmp($source,$destination)!= 0) {

    if(GenerateDirectRoute($source, $destination))
         return GenerateDirectRoute($source, $destination);
    else
         return GenerateIndirectRoute($source, $destination);
  }
  else
    echo "Source and destination are same! \n";
}

function GenerateDirectRoute($source, $destination) {
   global $DirectRouteFound_flag;
   $file_handler = fopen("data/locations.json","r");
   $DirectRoutes = array();
   $locations_json = fread($file_handler, filesize("data/locations.json"));
   $Locations = json_decode($locations_json);
   //echo "Calculating direct route\n";
   $sourceBusStops = array();
   $destinationBusStops = array();
   for($i = 0; $i < count($Locations); $i++) {
      if(strcmp($source, $Locations[$i]->name) == 0) {
          $sourceBusStops = $Locations[$i]->busNumbers;
      }
      if(strcmp($destination,$Locations[$i]->name) == 0)
      {
          $destinationBusStops = $Locations[$i]->busNumbers;
      }
   }

   $PossibleDirectBusses = __::intersection($sourceBusStops, $destinationBusStops);
   if(count($PossibleDirectBusses) == 0){
      $DirectRouteFound_flag = false;
      return false;
   }

   for($i = 0; $i < count($PossibleDirectBusses); $i++)
   {
      if(!$DirectRouteFound_flag || true) { //remove the "| true" to get detailed route in case of direct route
         $newRoute = new DirectRoutesClass();
         $newRoute->number = $PossibleDirectBusses[$i];
         array_push($newRoute->route, $source);
         array_push($newRoute->route, $destination);
         array_push($DirectRoutes, $newRoute);
      }
      else{
         $newRoute = __::find($busses, function($bus) {
         if(strcmp($PossibleDirectBusses[$i],$bus->number) == 0)
             return $bus->route;
         });

         $SourceIndex = __::indexOf($newRoute->route, $source);
         array_splice($newRoute->route,0,$SourceIndex);
         $DestinationIndex = __::indexOf($newRoute->route, $destination);
         array_splice($newRoute->route,$DestinationIndex+1,count($newRoute->route));
         array_push($DirectRoutes,$newRoute);
      }
  }
  return json_encode($DirectRoutes);
}

function GenerateInDirectRoute($source, $destination) {

  $file_handler = fopen("data/locations.json","r");
  $DirectRoutes = array();
  $locations_json = fread($file_handler, filesize("data/locations.json"));
   //echo "Calculating indirect routes";

  $Locations = json_decode($locations_json);
     $IndirectRoutes = new IndirectRoutesClass();
  $sourceBusStopsI = array();
  $destinationBusStopsI = array();
  $SourceBusRoutes = array();
  $DestinationBusRoutes = array();
  $PossibleIntermediateLocations = array();
 

  for($i = 0; $i< count($Locations); $i++) {

    if(strcmp($source,$Locations[$i]->name) == 0)
      $sourceBusStopsI = $Locations[$i]->busNumbers;

    if(strcmp($destination, $Locations[$i]->name) == 0)
      $destinationBusStopsI = $Locations[$i]->busNumbers;
  }

  for($i = 0; $i < count($sourceBusStopsI); $i++) {

    for($j = 0; $j < count($busses); $j++) {

      if(strcmp($sourceBusStopsI[$i],$busses[$j]->number) == 0)
        array_push($SourceBusRoutes, $busses[$j]);

    }

  }
  for($i = 0; $i < count($destinationBusStopsI); $i++) {

    for($j = 0; $j < count($busses); $j++) {

      if(strcmp($destinationBusStopsI[$i],$busses[$j]->number) == 0)
        array_push($DestinationBusRoutes, $busses[$j]);

    }

  }


  for($i = 0; $i < count($SourceBusRoutes); $i++) {

    for($j = 0; $j < count($DestinationBusRoutes); $j++) {

      $PossibleIntermediateLocations = __::intersection($SourceBusRoutes[$i], $DestinationBusRoutes[$j]);

    }

  }

  //alert('source '+JSON.stringify(SourceBusRoutes)+' \ndest '+JSON.stringify(DestinationBusRoutes));

  $IntermediateLocations = __::uniq($PossibleIntermediateLocations);

  //alert(JSON.stringify(IntermediateLocations));

  for($i = 0; $i < count($IntermediateLocations); $i++) {

    $newRouteI = new IndirectRoutesClass();

    GenerateDirectRoute($source, $IntermediateLocations[i]);
    //$newRouteI->first = _duplicate($DirectRoutes);
    $newRouteI->first =  GenerateDirectRoute($source, $IntermediateLocations[i]);
    //$DirectRoutes = [];

    //GenerateDirectRoute($IntermediateLocations[$i], $destination);
    //$newRouteI->second = _duplicate($DirectRoutes);

    $newRouteI->second =  GenerateDirectRoute($IntermediateLocations[i],$destination);
    //$DirectRoutes = [];

    array_push($IndirectRoutes, $newRouteI);

  }

      return json_encode($IndirectRoutes);


}
/*
function Displayer() {

  global $DirectRouteFound_flag;
  $output = " ";

  if($DirectRouteFound_flag){

    for($i = 0; $i < count($DirectRoutes); $i++) {

      $output = $output."Number: ";
      $output = $output.$DirectRoutes[$i]->number."\n";
      $output = $output." Bus route: ";

      for($j = 0; $j < count($DirectRoutes[$i]->route); $j++) {

        $output = $output.$DirectRoutes[$i]->route[$j]."->";
      }

      $output = $output."\n";

    }

  }
else{
    $busses1 = [];
    $busses2 = [];

    for($i = 0; $i < count($IntermediateLocations) ; $i++) {

      $output = $output."To reach intermediate location: \n";
      $output = $output.$IntermediateLocations[$i]."\n";

      for($j = 0; $j < count($IndirectRoutes); $j++) {

        for($k = 0; $k < count($IndirectRoutes[$j]->first); $k++) {

          array_push($busses1,$IndirectRoutes[$j]->first[$k]->number);

        }

      }
      $output = $output.json_encode(_uniq($busses1))."\n";

      $output = $output."To reach destination: \n";

      for($j = 0; $j < count($IndirectRoutes); $j++) {

        for($k = 0; $k < count($IndirectRoutes[$j]->second); $k++) {

          array_push($busses2, $IndirectRoutes[$j]->second[$k]->number);

        }

      }
      $output = $output.json_encode(_uniq($busses2))."\n";

      $output = $output."\n";

    }

  }
  echo $output;

}
*/
echo GenerateRoute("bank","SC");
