<?php

// Utility classes
class Bus {
  public $index;
  public $number;
}

class BusRouteInfo {
  public $number;
  public $route;
  public function __construct() {
    $this->number = "";
    $this->route = array();
  }
  public function addRoute($newLocationInRoute) {
    array_push($this->route, $newLocationInRoute);
  }
}

class LocationBus {
  public $name;
  public $busNumbers;
  public function __construct() {
    $this->name = "";
    $this->busNumbers = array();
  }
  public function addNumber($newNumber) {
    array_push($this->busNumbers, "$newNumber");
  }
}

class DirectRoutesClass {
  public $number;
  public $route;
  public function __construct() {
    $this->number = "";
    $this->route = array();
  }
}

class IndirectRoutesClass {
  public $first;
  public $second;
  public function __construct() {
    $this->first = new DirectRoutesClass();
    $this->second = new DirectRoutesClass();
  }
}

?>
